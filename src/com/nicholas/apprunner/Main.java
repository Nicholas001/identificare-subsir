package com.nicholas.apprunner;

import com.nicholas.opperations.Opperation;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        int[] sirDeNumereBrut = {0, 1, -2, 3, -4, 5, -6, 7, 8, 9, -10, 11, -12, 13, -14, 15, -16, 17, 18, 19, -20};
        System.out.println("Array-ul initial este: " + '\n' + Arrays.toString(sirDeNumereBrut));

        Opperation obj = new Opperation();
        obj.creazaSubSir(sirDeNumereBrut);

    }
}



