package com.nicholas.opperations;

import java.util.ArrayList;

public class Opperation {

    public void creazaSubSir(int[] sirDeNumere) {
        ArrayList<Integer> sirDeNumereTest = new ArrayList<>();

        for (int x = 0; x < sirDeNumere.length; x++) {
            sirDeNumereTest.add(sirDeNumere[x]);
        }
        //  Se adauga inca un element in ArrayList, un ultim element care nu indeplineste conditia din if pentru a incheia bucla prin
        //  trecere prin else.
        sirDeNumereTest.add(sirDeNumere[sirDeNumere.length - 1] + 2);

        int indexStart = 0;
        int indexEnd = 0;
        int indexStartMax = 0;
        int indexEndMax = 0;

        for (int x = 1; x < sirDeNumereTest.size(); x++) {
            if ((sirDeNumereTest.get(x) < 0 && sirDeNumereTest.get(x - 1) >= 0) || (sirDeNumereTest.get(x) >= 0 && sirDeNumereTest.get(x - 1) < 0)) {
                indexEnd = x;
            } else {
                if ((indexEnd - indexStart) > (indexEndMax - indexStartMax)) {
                    indexEndMax = indexEnd;
                    indexStartMax = indexStart;
                }
                indexStart = x;
            }
        }

        System.out.println("Cel mai lung subsir in care apar alternativ un numar pozitiv si unul negativ sau invers este: ");
        for (int x = indexStartMax; x <= indexEndMax; x++)
            System.out.print(" " + sirDeNumereTest.get(x));
    }
}

